# [MovieApp](https://learning-next-amber-ten.vercel.app/)

## ![Imágen de la aplicación](/public/image.png)

## ToDo

- [x] Revisar nested layouts
- [x] Revisar fetching de datos para extraer imágenes
- [x] Corregir grid de imágenes, no responsive.
- [x] Añadido loading page
- [x] Añadida paginación
- [x] Buscar forma de hacer paginación server-side (preguntar César)
- [x] corregir estilos MovieDetails (centrar)
- [x] corregir: el fetch de datos se lanza dos veces
- [x] cambiar /discover/page.jsx --> añadir barra búsqueda
- [x] dar estilos a SignupForm
- [x] añadir mutations en los forms
- [x] Funcionalidad a SearchBar
- [x] Corregir paginación
- [ ] Probar infinite scroll de tankstack (nueva rama)
- [ ] Buscar forma de persistir el estado entre páginas y entre sesiones (zustand/useContext???)
- [ ] Investigar más providers Next-Auth

## Sugerencias César

- [x] Code format (Prettier)
- [x] string props format
- [x] wrapper component (popular,topRated,upcoming)
- [x] infinite load
- [x] include axios
- [x] create API class for data fetching
- [x] uso de .env.local en raíz proyecto para API_KEY
- [x] Error management (useState --> fetchError)
- [x] Corregir loading
- [x] Corregir paginación
- [x] Estudiar tanstack query
- [x] Añadir tanstack query (añadida paginación)
- [x] MovieGrid 6/7 columnas
- [x] Skeleton MovieGrid
- [x] Componente Movies. Usar `<Image/>` de Next
- [x] paginación a través de url (?page=)
- [ ] Test infinite load react-query
- [x] añadir fav (al componente `<Movie/>`) y funcionalidad
- [x] Estudiar next-auth
