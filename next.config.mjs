/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['image.tmdb.org', 'avatars.githubusercontent.com'],
  },
}

export default nextConfig
