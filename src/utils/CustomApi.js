import axios from 'axios'

class CustomApi {
  constructor(API_URL) {
    this.API_URL = API_URL

    this.axiosInstance = axios.create({
      baseURL: this.API_URL,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${process.env.NEXT_PUBLIC_AUTHORIZATION}`,
      },
    })
  }

  get = async (url, page) => {
    return await this.axiosInstance
      .get(`${url}?language=en-US&page=${page}`)
      .then((res) => res.data)
      .catch((err) => console.error(err))
  }
  getMovieById = async (id) => {
    return await this.axiosInstance.get(id).then((response) => {
      return response.data
    })
  }
  getMovieByTitle = async (title = '', page = 1) => {
    const axiosInstance = axios.create({
      baseURL: 'https://api.themoviedb.org/3/search/',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${process.env.NEXT_PUBLIC_AUTHORIZATION}`,
      },
    })
    return await axiosInstance
      .get(
        `movie?query=${title}&include_adult=false&language=en-US&page=${page}`
      )
      .then((response) => {
        return response.data
      })
  }
}

const api = new CustomApi('https://api.themoviedb.org/3/movie/')
export default api
