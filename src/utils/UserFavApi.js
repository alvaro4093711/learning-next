import axios from 'axios'
class UserFavApi {
  constructor(API_URL) {
    this.API_URL = API_URL
    this.axiosInstance = axios.create({
      baseURL: this.API_URL,
    })
  }

  getUser = async (username) => {
    await this.axiosInstance
      .get('/users')
      .then((response) => {
        return response
      })
      .catch((error) => {
        return error
      })
  }

  postUser = async (user) => {
    const res = await this.axiosInstance.post('/users', user).catch((error) => {
      return error
    })
    return res.data
  }
  getUserFav = async (user) => {
    const res = await this.axiosInstance.get(`/favs`).catch((error) => {
      error
    })
    const favs = res.data.filter((fav) => fav.user_name === user.name)
    return favs
  }
  postUserFav = async (user, movie) => {
    const body = {
      id: movie.id,
      user_name: user.name,
      title: movie.title,
      poster_path: movie.poster_path,
    }
    const res = await await this.axiosInstance
      .post(`/favs`, body)
      .catch((error) => {
        return error
      })

    return res.data
  }
  deleteUserFav = async (movie) => {
    const res = await this.axiosInstance
      .delete(`/favs?id=${movie.id}`)
      .catch((error) => {
        return error
      })
    return res
  }
}

const userFavApi = new UserFavApi('http://localhost:4000/')
export default userFavApi
