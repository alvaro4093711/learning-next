import { MovieGrid } from '@/components/MovieGrid'

export default function TopRated() {
  return <MovieGrid urlParam='top_rated' />
}
