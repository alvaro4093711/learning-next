'use client'
import { useQuery } from '@tanstack/react-query'
import userFavApi from '@/utils/UserFavApi'
import { useSession } from 'next-auth/react'
import { Movie } from '@/components/Movie'
import { MovieSkeleton } from '@/components/MovieSkeleton'
import { FetchError } from '@/components/FetchError'
function FavouritesPage() {
  const { data: session } = useSession()
  const user = session?.user
  const query = useQuery({
    queryKey: ['favourites', user],
    queryFn: () => userFavApi.getUserFav(user),
  })
  return (
    <div className='p-48'>
      {query.isError && <FetchError />}
      <div className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-10'>
        {query.isLoading &&
          Array.from({ length: 12 }).map((_, index) => (
            <MovieSkeleton key={index} />
          ))}
      </div>
      {query.isSuccess && (
        <div className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-10'>
          {query.data.map((movie) => (
            <Movie key={movie.id} movie={movie} />
          ))}
        </div>
      )}
    </div>
  )
}
export default FavouritesPage
