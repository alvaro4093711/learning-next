import { Inter } from 'next/font/google'
import './globals.css'
import TanstackProvider from '@/utils/TanstackProvider'
import AuthProvider from '@/utils/AuthProvider'
import Header from '@/components/Header'
export const metadata = {
  title: `Movie App`,
  description: 'A simple movie app',
  icons: {
    icon: '/icon.png',
  },
}
const inter = Inter({ subsets: ['latin'] })

export default function RootLayout({ children }) {
  return (
    <html lang='en'>
      <body className={`${inter.className} min-h-screen scroll-smooth`}>
        <AuthProvider>
          <TanstackProvider>
            <Header />
            {children}
          </TanstackProvider>
        </AuthProvider>
      </body>
    </html>
  )
}
