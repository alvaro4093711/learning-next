import { SignupForm } from '../../components/SignupForm'
export default function Singup() {
  return (
    <>
      <div className='flex h-screen w-full items-center justify-center'>
        <SignupForm />
      </div>
    </>
  )
}
