import { MovieGrid } from '../../../components/MovieGrid'

export default function Popular() {
  return <MovieGrid sectionTitle='Popular Movies' urlParam='popular' />
}
