'use client'
import { useQuery } from '@tanstack/react-query'
import { useSearchParams } from 'next/navigation'
import api from '@/utils/CustomApi'
import { FetchError } from '@/components/FetchError'
import { MovieSkeleton } from '@/components/MovieSkeleton'
import { Movie } from '@/components/Movie'
import { Pagination } from '@/components/Pagination'
export default function Page() {
  const page = useSearchParams().get('page')
  const title = useSearchParams().get('title')

  const query = useQuery({
    queryKey: ['movie', title, page],
    queryFn: () => api.getMovieByTitle(title, page),
  })

  return (
    <div className='flex flex-col items-center'>
      <h1 className='text-3xl'>
        Results for <span className='italic'>{title}</span>
      </h1>
      <div className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-10'>
        {query.isLoading &&
          Array.from({ length: 18 }).map((_, index) => (
            <MovieSkeleton key={index} />
          ))}
      </div>
      {query.isSuccess && (
        <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-12 my-16'>
          {query.data.results.map((movie) => (
            <Movie key={movie.id} movie={movie} />
          ))}
        </div>
      )}
      {query.isError && <FetchError />}
      <Pagination
        page={page}
        title={title}
        totalPages={query.data ? query.data.total_pages : 1}
      />
    </div>
  )
}
