import { SearchBar } from '@/components/SearchBar'
export default function DiscoverPage() {
  return (
    <>
      <div className='flex flex-col items-center justify-center gap-12 min-h-screen'>
        <div className='flex flex-col items-center justify-center'>
          <h1 className='text-6xl text-center p-8'>Discover new movies</h1>
          <SearchBar />
        </div>
      </div>
    </>
  )
}
