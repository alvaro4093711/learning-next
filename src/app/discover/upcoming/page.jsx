import { MovieGrid } from '../../../components/MovieGrid'
export default function UpcomingPage() {
  return <MovieGrid sectionTitle='Upcoming Movies' urlParam='upcoming' />
}
