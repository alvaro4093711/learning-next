import { NextResponse } from 'next/server'

export async function middleware(req) {
  const cookie = await req.cookies.get('next-auth.session-token')
  const url = req.nextUrl.clone()
  if (!cookie) {
    url.pathname = '/login'
    return NextResponse.redirect(url)
  }
  return NextResponse.next()
}

export const config = {
  matcher: '/favourites',
}
