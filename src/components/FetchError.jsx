import Image from 'next/image'
export const FetchError = () => (
  <div className='flex flex-col h-screen items-center justify-center'>
    <Image src='/whaleError.png' alt='Error Image' width={703} height={439} />
    <p className='md:text-xl text-center p-5'>
      Ops! Something went wrong. Please try again later.
    </p>
  </div>
)
