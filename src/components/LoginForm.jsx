'use client'
import Link from 'next/link'
import { useForm } from 'react-hook-form'
import { useState } from 'react'
import { signIn } from 'next-auth/react'
import styles from '../styles/LoginForm.module.css'
import { ProviderButtons } from './ProviderButtons'

export const LoginForm = () => {
  const [showPassword, setShowPassword] = useState(false)
  const [inputType, setInputType] = useState('password')
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = async (data) => {
    const response = await signIn('credentials', {
      username: data.username,
      password: data.password,
      redirect: false,
    })
    if (response.error) {
      alert('Invalid credentials')
      window.location.reload()
    } else if (response.ok) {
      window.location.href = '/discover'
    }
  }

  return (
    <div className=' flex flex-col items-center gap-8'>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className='flex flex-col gap-4'>
          <label className={styles.label} htmlFor='username'>
            <b>Username:</b>
          </label>
          <input
            autoComplete='off'
            className={styles.input}
            type='text'
            name='username'
            {...register('username', {
              required: 'This field is required',
              minLength: {
                value: 3,
                message: 'Username must have at least 3 characters',
              },
            })}
          />
          {errors.username && (
            <span className='text-red-400'>{errors.username.message}</span>
          )}
          <label className={styles.label} htmlFor='password'>
            <b>Password:</b>
          </label>
          <div className='relative flex'>
            <input
              className={styles.input}
              type={inputType}
              name='password'
              {...register('password', {
                required: 'This field is required',
                minLength: {
                  value: 6,
                  message: 'Password must have at least 6 characters',
                },
              })}
            />
            <a
              href='#'
              onClick={(e) => {
                e.preventDefault()
                setShowPassword((showPassword) => !showPassword)
                setInputType(showPassword ? 'password' : 'text')
              }}
              className='absolute right-0 bottom-2'
            >
              {showPassword ? (
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  width='24'
                  height='24'
                  viewBox='0 0 24 24'
                >
                  <path
                    fill='currentColor'
                    d='M2 5.27L3.28 4L20 20.72L18.73 22l-3.08-3.08c-1.15.38-2.37.58-3.65.58c-5 0-9.27-3.11-11-7.5c.69-1.76 1.79-3.31 3.19-4.54zM12 9a3 3 0 0 1 3 3a3 3 0 0 1-.17 1L11 9.17A3 3 0 0 1 12 9m0-4.5c5 0 9.27 3.11 11 7.5a11.79 11.79 0 0 1-4 5.19l-1.42-1.43A9.862 9.862 0 0 0 20.82 12A9.821 9.821 0 0 0 12 6.5c-1.09 0-2.16.18-3.16.5L7.3 5.47c1.44-.62 3.03-.97 4.7-.97M3.18 12A9.821 9.821 0 0 0 12 17.5c.69 0 1.37-.07 2-.21L11.72 15A3.064 3.064 0 0 1 9 12.28L5.6 8.87c-.99.85-1.82 1.91-2.42 3.13'
                  />
                </svg>
              ) : (
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  width='24'
                  height='24'
                  viewBox='0 0 24 24'
                >
                  <path
                    fill='currentColor'
                    d='M12 9a3 3 0 0 1 3 3a3 3 0 0 1-3 3a3 3 0 0 1-3-3a3 3 0 0 1 3-3m0-4.5c5 0 9.27 3.11 11 7.5c-1.73 4.39-6 7.5-11 7.5S2.73 16.39 1 12c1.73-4.39 6-7.5 11-7.5M3.18 12a9.821 9.821 0 0 0 17.64 0a9.821 9.821 0 0 0-17.64 0'
                  />
                </svg>
              )}
            </a>
          </div>

          {errors.password && (
            <span className='text-red-400'>{errors.password.message}</span>
          )}
          <div className='flex flex-col w-full items-center'>
            <p>
              Don&apos;t have an account?{' '}
              <Link href='/signup' className='text-blue-500'>
                Sign up
              </Link>
            </p>
            <div className='flex gap-5'>
              <button
                className='bg-blue-700 p-3 rounded-md font-semibold hover:bg-blue-500 mt-12 w-32'
                onSubmit={handleSubmit(onSubmit)}
              >
                Login
              </button>
            </div>
          </div>
        </div>
      </form>
      <ProviderButtons />
      <a href='/discover' className='font-thin text-white/50 hover:text-white'>
        Continue as a guest
      </a>
    </div>
  )
}
