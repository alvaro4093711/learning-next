export const MovieDetailSkeleton = () => {
  return (
    <div className='flex flex-col lg:grid lg:grid-cols-12 lg:px-18 items-center justify-center  gap-16 py-8 bg-slate-300 animate-pulse rounded-md px-6'>
      <div className='lg:col-span-4'>
        <div className='bg-gray-400 animate-pulse rounded-md w-72 h-96'></div>
      </div>
      <div className='flex flex-col col-span-8 min-w-72 lg:p-0 gap-10 lg:gap-16'>
        <div className='bg-gray-400 animate-pulse w-full md:w-1/2 h-8 lg:h-14 rounded-md'></div>
        <div className='grid w-full h-8 lg:h-40 rounded-md gap-3 lg:gap-0'>
          <div className='bg-gray-400 animate-pulse rounded-md h-4 '></div>
          <div className='bg-gray-400 animate-pulse rounded-md h-4 w-5/6'></div>
          <div className='hidden lg:block bg-gray-400 animate-pulse rounded-md h-4 w-4/6'></div>
          <div className='hidden lg:block bg-gray-400 animate-pulse rounded-md h-4 w-3/6'></div>
          <div className='hidden lg:block bg-gray-400 animate-pulse rounded-md h-4 w-5/6'></div>
        </div>
        <div className='flex justify-between gap-6'>
          <div className='bg-gray-400 animate-pulse w-1/4 rounded-md h-4 lg:h-8'></div>
          <div className='bg-gray-400 animate-pulse w-1/4 rounded-md h-4 lg:h-8'></div>
          <div className='bg-gray-400 animate-pulse w-1/4 rounded-md h-4 lg:h-8'></div>
        </div>
      </div>
    </div>
  )
}
