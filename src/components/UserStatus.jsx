'use client'
import { signOut, useSession } from 'next-auth/react'
import Image from 'next/image'
export const UserStatus = () => {
  const { data: session, status } = useSession()
  return (
    <div className='flex items-center p-3 gap-8'>
      {/* {status === 'authenticated' && (
        <p className='w-16 text-right'>{session.user.name}</p>
      )} */}
      {status === 'authenticated' ? (
        <div href='#' className='relative group p-3'>
          <Image
            src={session.user.image}
            alt='Avatar image'
            width={50}
            height={50}
            className='rounded-full'
          />
          <ul className='absolute p-5 right-0 top-12 hidden hover:block group-hover:block'>
            <li>
              <a href='/favourites' className='hover:text-blue-500'>
                Favourites
              </a>
            </li>
            <li>
              <a href='/profile' className='hover:text-blue-500'>
                Profile
              </a>
            </li>
            <li>
              <a
                onClick={() => {
                  signOut({ callbackUrl: '/' })
                }}
                className='hover:text-blue-500 cursor-pointer'
              >
                Sign out
              </a>
            </li>
          </ul>
        </div>
      ) : (
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='50'
          height='50'
          viewBox='0 0 256 256'
        >
          <path
            fill='#ffffff'
            d='M128 24a104 104 0 1 0 104 104A104.11 104.11 0 0 0 128 24M74.08 197.5a64 64 0 0 1 107.84 0a87.83 87.83 0 0 1-107.84 0M96 120a32 32 0 1 1 32 32a32 32 0 0 1-32-32m97.76 66.41a79.66 79.66 0 0 0-36.06-28.75a48 48 0 1 0-59.4 0a79.66 79.66 0 0 0-36.06 28.75a88 88 0 1 1 131.52 0'
          />
        </svg>
      )}
    </div>
  )
}
