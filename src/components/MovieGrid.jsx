'use client'
import { useSearchParams } from 'next/navigation'
import { FetchError } from './FetchError'
import { useQuery, keepPreviousData } from '@tanstack/react-query'
import api from '../utils/CustomApi'
import { Pagination } from './Pagination'
import { Movie } from './Movie'
import { MovieSkeleton } from './MovieSkeleton'

export const MovieGrid = ({ urlParam, sectionTitle }) => {
  //states

  const searchParams = useSearchParams()
  const page = searchParams.get('page') || 1
  const title = searchParams.get('title') || ''
  const movieQuery = useQuery({
    queryKey: ['movies', page],
    queryFn: () => api.get(urlParam, page),
    placeholderData: keepPreviousData,
  })

  return (
    <div className='flex flex-col items-center justify-center mt-10 px-8'>
      <h1 className='text-4xl sm:text-5xl md:text-6xl font-light mb-20'>
        {sectionTitle}
      </h1>
      <div className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-10'>
        {movieQuery.isLoading &&
          Array.from({ length: 18 }).map((_, index) => (
            <MovieSkeleton key={index} />
          ))}
      </div>
      {movieQuery.error && <FetchError />}
      {movieQuery.isSuccess && (
        <div className='grid place-items-center text-center'>
          {movieQuery.isSuccess && (
            <div className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-10'>
              {movieQuery.data.results.map((movie) => (
                <Movie key={movie.id} movie={movie} />
              ))}
            </div>
          )}
        </div>
      )}
      <Pagination
        totalPages={movieQuery.data ? movieQuery.data.total_pages : 1}
      />
    </div>
  )
}
