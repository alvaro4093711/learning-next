'use client'
import { useForm } from 'react-hook-form'
import { useRouter } from 'next/navigation'

export const SearchBar = () => {
  const router = useRouter()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = async (data) => {
    router.push(`/discover/search?title=${data.title}&page=1`)
  }
  return (
    <div className='grid place-items-center p-20 text-center'>
      <form
        className='flex gap-3 items-center justify-center'
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className='flex flex-col'>
          <div className='flex'>
            <input
              type='text'
              placeholder='Search movie'
              className='p-3 rounded-md rounded-r-none placeholder:text-gray-400 text-gray-800'
              {...register('title', {
                required: 'This field is required',
                minLength: {
                  value: 3,
                  message: 'Minimum length is 3',
                },
              })}
            />

            <button className='bg-blue-700 p-3 rounded-md rounded-l-none font-semibold hover:bg-blue-500'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                fill='none'
                viewBox='0 0 24 24'
                strokeWidth='1.5'
                stroke='currentColor'
                className='w-6 h-6'
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z'
                />
              </svg>
            </button>
          </div>
          {errors.title && (
            <span className='text-red-500 p-3'>{errors.title.message}</span>
          )}
        </div>
      </form>
    </div>
  )
}
