'use client'
import { useRouter, useSearchParams, usePathname } from 'next/navigation'
import { useCallback } from 'react'
export const Pagination = ({ totalPages }) => {
  const router = useRouter()
  const searchParams = useSearchParams()
  const pathname = usePathname()

  const createQueryString = useCallback(
    (name, value) => {
      const params = new URLSearchParams(searchParams.toString())
      params.set(name, value)
      return params.toString()
    },
    [searchParams]
  )
  const page = searchParams.get('page')
  return (
    <div className='flex flex-col gap-3 items-center justify-center p-6'>
      <div className='flex gap-3'>
        <button
          className='bg-blue-700 p-3 rounded-md font-semibold hover:bg-blue-500'
          onClick={() =>
            router.push(
              pathname + '?' + createQueryString('page', Number(page) - 1)
            )
          }
          disabled={Number(page) === 1}
        >
          Prev
        </button>
        <button
          className='flex gap-3 items-center justify-center bg-blue-700 p-3 rounded-md font-semibold hover:bg-blue-500'
          onClick={() =>
            router.push(
              pathname + '?' + createQueryString('page', Number(page) + 1)
            )
          }
          disabled={Number(page) === totalPages}
        >
          Next
        </button>
      </div>
      <p className='font-light'>
        Current page: {searchParams.get('page') || 1} of {totalPages}
      </p>
    </div>
  )
}
