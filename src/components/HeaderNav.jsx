'usue client'
import Link from 'next/link'
import { UserStatus } from './UserStatus'
import { useSession } from 'next-auth/react'
import { useState } from 'react'
import styles from '@/styles/Header.module.css'
export const HeaderNav = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const { data: session } = useSession()
  return (
    <div className='flex flex-col items-start w-full p-3'>
      <a
        href=''
        onClick={(e) => {
          e.preventDefault()
          setIsMenuOpen((isMenuOpen) => !isMenuOpen)
        }}
        className='lg:hidden'
      >
        {!isMenuOpen ? (
          <svg
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'
            height={40}
            width={40}
            strokeWidth='1.5'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              d='M6 18L18 6M6 6l12 12'
            />
          </svg>
        ) : (
          <svg
            xmlns='http://www.w3.org/2000/svg'
            width='40'
            height='40'
            viewBox='0 0 24 24'
          >
            <path fill='#ffffff' d='M3 18v-2h18v2zm0-5v-2h18v2zm0-5V6h18v2z' />
          </svg>
        )}
      </a>
      <nav
        className={
          isMenuOpen
            ? 'hidden flex-col lg:flex lg:flex-row w-full justify-end items-center gap-3'
            : 'flex flex-col lg:flex-row w-full justify-end items-center gap-3 lg:flex'
        }
      >
        <Link className={styles.link} href='/'>
          Home
        </Link>
        <Link className={styles.link} href='/discover'>
          Search
        </Link>
        <Link className={styles.link} href='/discover/popular?page=1'>
          Popular
        </Link>
        <Link className={styles.link} href='/discover/upcoming?page=1'>
          Upcoming
        </Link>
        {session && (
          <Link className={styles.link} href='/favourites'>
            Favourites
          </Link>
        )}
        {!session && (
          <Link
            href={session ? '/api/auth/signout' : '/login'}
            className={styles.primaryLink}
          >
            Sign in
          </Link>
        )}
        <UserStatus />
      </nav>
    </div>
  )
}
