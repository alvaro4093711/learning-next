import Image from 'next/image'
import { FavButton } from './FavButton'
import { useSession } from 'next-auth/react'
export const Movie = ({ movie }) => {
  const { data: session } = useSession()
  return (
    <div className='hover:scale-105 animation duration-300'>
      {movie && (
        <div className='relative'>
          <a
            href={`/discover/movie/${movie.id}`}
            className='grid items-center justify-center h-[400px]'
          >
            <Image
              src={
                movie.poster_path
                  ? `https://image.tmdb.org/t/p/w500/${movie.poster_path}`
                  : '/no-image.png'
              }
              alt={movie.title}
              className='rounded-md max-w-60 h-[360px]'
              width={500}
              height={750}
              priority
            />
            <p className='text-center max-w-60'>{movie.title}</p>
          </a>
          {session && <FavButton movie={movie} />}
        </div>
      )}
    </div>
  )
}
