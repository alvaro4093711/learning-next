'use client'
import { useParams } from 'next/navigation'
import { useQuery } from '@tanstack/react-query'
import { MovieDetailSkeleton } from './MovieDetailSkeleton'
import api from '../utils/CustomApi'
import Image from 'next/image'

export function MovieDetails() {
  const { movieId } = useParams()
  const query = useQuery({
    queryKey: ['movie', movieId],
    queryFn: () => api.getMovieById(`${movieId}`),
  })

  return (
    <>
      {query.isLoading && (
        <div className='flex flex-col items-center justify-center h-screen'>
          <MovieDetailSkeleton />
        </div>
      )}
      {query.isSuccess && (
        <div className='flex flex-col mt-20 lg:grid lg:grid-cols-12 w-3/2 items-center justify-center lg:h-screen lg:mt-0'>
          <div className='lg:col-span-4 flex justify-center'>
            <Image
              src={`https://image.tmdb.org/t/p/w500${query.data.poster_path}`}
              alt={query.data.title}
              width={500}
              height={750}
              className='rounded-md max-w-72'
              priority
            />
          </div>
          <div className='col-span-8 p-24 lg:p-0'>
            <h1 className='text-4xl text-center lg:text-left sm:text-5xl md:text-6xl mb-24 font-semibold'>
              {query.data.title}
            </h1>
            <h6 className='pb-4 text-xl font-semibold'>Overview</h6>
            <p className='md:max-w-[500px] lg:max-w-[700px]'>
              {query.data.overview}
            </p>
            <div className='flex gap-4'>
              <h6 className='py-4 text-lg font-semibold'>
                Year:
                <span className='font-light px-1'>
                  {query.data.release_date &&
                    query.data.release_date.split('-')[0]}
                </span>
              </h6>
              <h6 className='py-4 text-lg font-semibold'>
                Runtime:
                <span className='font-light px-1'>{query.data.runtime}</span>
              </h6>
              <h6 className='py-4 text-lg font-semibold'>
                Avg. votes:
                <span className='font-light px-1'>
                  {query.data.vote_average}
                </span>
              </h6>
            </div>
          </div>
        </div>
      )}
    </>
  )
}
