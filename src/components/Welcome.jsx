export const Welcome = () => {
  return (
    <main className='w-full h-screen flex flex-col items-center justify-center'>
      <h1 className='text-6xl'>Welcome to MovieApp</h1>
      <p className='p-6'>Here you will find the best movies to watch</p>
    </main>
  )
}
