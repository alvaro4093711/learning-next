export const MovieSkeleton = () => {
  return (
    <div className='flex flex-col items-center justify-center p-3 bg-slate-100 animate-pulse rounded-md w-[240px] h-[400px]'>
      <div className='animate-pulse bg-gray-300 rounded-md object-cover w-full h-full' />
      <div className='animate-pulse bg-gray-300 rounded-md h-10 w-full mt-2' />
    </div>
  )
}
