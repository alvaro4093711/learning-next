import Link from 'next/link'
export const Button = ({ href, text = 'Button', className, onClick }) => {
  return (
    <Link className={className} href={href} onClick={onClick}>
      {text}
    </Link>
  )
}
