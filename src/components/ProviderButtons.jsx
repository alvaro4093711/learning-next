import { signIn } from 'next-auth/react'
export const ProviderButtons = () => {
  return (
    <div className='grid gap-4'>
      <button
        className='bg-white hover:bg-white/80 text-black font-bold py-2 px-4 rounded'
        onClick={() =>
          signIn('github', { callbackUrl: 'http://localhost:3000' })
        }
      >
        Sign in with GitHub
      </button>
    </div>
  )
}
