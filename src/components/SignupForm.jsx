'use client'
import { useForm } from 'react-hook-form'
import { useMutation } from '@tanstack/react-query'
import { useState } from 'react'

import userFavApis from '../utils/UserFavApi'

import styles from '../styles/LoginForm.module.css'
export const SignupForm = () => {
  const [showPassword, setShowPassword] = useState(false)
  const [inputType, setInputType] = useState('password')
  const {
    register,
    reset,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const mutation = useMutation({
    mutationFn: (user) => {
      return userFavApis.postUser(user)
    },
  })

  const onSubmit = async (values) => {
    mutation.mutateAsync(values).then(() => {
      if (mutation.isSuccess) {
        alert('User created successfully')
      } else if (mutation.isError) {
        alert('Error creating user')
      }
      reset()
    })
  }
  const onError = (errors) => console.log(errors)

  return (
    <form onSubmit={handleSubmit(onSubmit, onError)}>
      <div className='flex flex-col gap-2'>
        <label htmlFor='name' className={styles.label}>
          Name:
        </label>
        <input
          type='text'
          name='username'
          id='username'
          className={styles.input}
          {...register('name', {
            required: 'This field is required',
            minLength: {
              value: 3,
              message: 'Name must have at least 3 characters',
            },
          })}
        />
        {errors.name && (
          <span className='text-red-400'>{errors.name.message}</span>
        )}
        <label htmlFor='email' className={styles.label}>
          Email:
        </label>
        <input
          type='email'
          name='email'
          id='email'
          className={styles.input}
          {...register('email', {
            required: 'This field is required',
            pattern: {
              value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
              message: 'Invalid email address',
            },
          })}
        />
        {errors.email && (
          <span className='text-red-400'>{errors.email.message}</span>
        )}
        <label htmlFor='password' className={styles.label}>
          Password:
        </label>
        <div className='relative flex'>
          <input
            className={styles.input}
            type={inputType}
            name='password'
            {...register('password', {
              required: 'This field is required',
              minLength: {
                value: 8,
                message: 'Password must have at least 8 characters',
              },
            })}
          />
          <a
            href='#'
            onClick={(e) => {
              e.preventDefault()
              setShowPassword((showPassword) => !showPassword)
              setInputType(showPassword ? 'password' : 'text')
            }}
            className='absolute right-0 bottom-2'
          >
            {showPassword ? (
              <svg
                xmlns='http://www.w3.org/2000/svg'
                width='24'
                height='24'
                viewBox='0 0 24 24'
              >
                <path
                  fill='currentColor'
                  d='M2 5.27L3.28 4L20 20.72L18.73 22l-3.08-3.08c-1.15.38-2.37.58-3.65.58c-5 0-9.27-3.11-11-7.5c.69-1.76 1.79-3.31 3.19-4.54zM12 9a3 3 0 0 1 3 3a3 3 0 0 1-.17 1L11 9.17A3 3 0 0 1 12 9m0-4.5c5 0 9.27 3.11 11 7.5a11.79 11.79 0 0 1-4 5.19l-1.42-1.43A9.862 9.862 0 0 0 20.82 12A9.821 9.821 0 0 0 12 6.5c-1.09 0-2.16.18-3.16.5L7.3 5.47c1.44-.62 3.03-.97 4.7-.97M3.18 12A9.821 9.821 0 0 0 12 17.5c.69 0 1.37-.07 2-.21L11.72 15A3.064 3.064 0 0 1 9 12.28L5.6 8.87c-.99.85-1.82 1.91-2.42 3.13'
                />
              </svg>
            ) : (
              <svg
                xmlns='http://www.w3.org/2000/svg'
                width='24'
                height='24'
                viewBox='0 0 24 24'
              >
                <path
                  fill='currentColor'
                  d='M12 9a3 3 0 0 1 3 3a3 3 0 0 1-3 3a3 3 0 0 1-3-3a3 3 0 0 1 3-3m0-4.5c5 0 9.27 3.11 11 7.5c-1.73 4.39-6 7.5-11 7.5S2.73 16.39 1 12c1.73-4.39 6-7.5 11-7.5M3.18 12a9.821 9.821 0 0 0 17.64 0a9.821 9.821 0 0 0-17.64 0'
                />
              </svg>
            )}
          </a>
        </div>
        {errors.password && (
          <span className='text-red-400'>{errors.password.message}</span>
        )}
        <div className='w-full flex justify-center'>
          <button
            type='submit'
            className='bg-white/90 text-black hover:bg-white p-3 rounded-md font-semibold mt-12 w-32 text-center'
          >
            Submit
          </button>
        </div>
      </div>
    </form>
  )
}
