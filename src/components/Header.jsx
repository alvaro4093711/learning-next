'use client'
import { HeaderLogo } from './HeaderLogo'
import { HeaderNav } from './HeaderNav'
const Header = () => {
  return (
    <header className='flex justify-between'>
      <HeaderLogo />
      <HeaderNav />
    </header>
  )
}
export default Header
